﻿using KadamFruitVeggieStore.Models;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KadamFruitVeggieStore.Services
{
    public class ProductsService : IProductsService
    {
        public List<BsonDocument> Get()
        {
            var connectionString = "mongodb://localhost:27017";
            Product model = new Product();
            IMongoClient client = new MongoClient(connectionString);
            IMongoDatabase db = client.GetDatabase("admin");
            var collection = db.GetCollection<BsonDocument>("products");
            var documents = collection.Find(_ => true).ToList();

            return documents;
        }
    }
    //public Product Post(int id)
    //{
    //    return null;
    //}
}

