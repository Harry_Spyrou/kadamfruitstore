﻿using KadamFruitVeggieStore.Services;
using Microsoft.AspNetCore.Mvc;
using System;

namespace KadamFruitVeggieStore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {

        private readonly IProductsService _productsService;

        public ProductsController(IProductsService productsService)
        {
            _productsService = productsService;
        }
        // GET: api/Products
        [HttpGet]
        public IActionResult Get()
        {
            var records = _productsService.Get();

            if(records != null)
            {
                return Ok(records);
            }
            else
            {
                return NotFound();
            }
        }

        // GET: api/Products/5
        //[HttpGet("{id}", Name = "Get")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // POST: api/Products
        [HttpPost]
        public string Post([FromBody] string value)
        {
            if (value != null)
            {
                return "Thank you";
            }
            else
            {
                throw new Exception("value is null");
            }
        }

        // PUT: api/Products/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
