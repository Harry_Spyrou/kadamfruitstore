﻿using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace KadamFruitVeggieStore.Models
{
    public class Product
    {
        public IMongoCollection<BsonDocument> ProductsList {get; set;}
    }
}
